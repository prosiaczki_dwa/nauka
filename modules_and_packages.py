# import the library
import urllib

# use it
#urllib.urlopen(...)

print(dir(urllib))
#import foo.bar
#from foo import bar

import foo.abcd
foo.abcd.my_f()

import re
list_of_re_dir = dir(re)
print(dir(re))

print(sorted(list_of_re_dir))
res = [k for k in list_of_re_dir if 'find' in k]
print(res)