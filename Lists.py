print("This line will be printed. I love Agata.")
print("lalalala, I've found out that polish letters won't go")
x=1
if x==1:
    print("x is 1")
myint = 7
print(myint)

myfloat=7.0
print(myfloat)
myfloat = float(7)
print(myfloat)
print("so float gives a comma")

mystring = 'hello'
print(mystring)
mystring = "hello"
print(mystring)
mystring = "Don't worry about apostrophes"
print(mystring)

one = 1
two = 2
three = one + two
print(three)

hello = "hello"
world = "world"
helloworld = hello + " " + world
print(helloworld)

a, b = 3, 4
print(a,b)