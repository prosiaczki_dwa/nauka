def my_function():
    print("Hello From My Function!")
    
def my_function_with_args(username, greeting):
    print("Hello, %s , From My Function!, I wish you %s"%(username, greeting))
    
def array(object_a):
    pass

my_function_with_args("John Doe", "a great year!")

def sum_two_numbers(a, b):
    return a + b
x = sum_two_numbers(1,2)

print(x)