#create 2 lists
height = [1.87,  1.87, 1.82, 1.91, 1.90, 1.85]
weight = [81.65, 97.52, 95.25, 92.98, 86.18, 88.45]

import numpy as np
# Create 2 numpy arrays from height and weight
np_height = np.array(height)
np_weight = np.array(weight)

bmi = np_weight / np_height ** 2
print(bmi)

# For a boolean response
a = bmi > 23
# Print only those observations above 23
b= bmi[bmi > 26]
print(bmi)
print(b)
print(a)