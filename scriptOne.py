print("This line will be printed. I love Agata.")
print("lalalala, I've found out that polish letters won't go")
x=1
if x==1:
    print("x is 1")
myint = 7
print(myint)

myfloat=7.0
print(myfloat)
myfloat = float(7)
print(myfloat)
print("so float gives a comma")

mystring = 'hello'
print(mystring)
mystring = "hello"
print(mystring)
mystring = "Don't worry about apostrophes"
print(mystring)

one = 1
two = 2
three = one + two
print(three)

hello = "hello"
world = "world"
helloworld = hello + " " + world
print(helloworld)

a, b = 3, 4
print(a,b)

#lists
mylist = [] #first we have to define list
mylist.append(1)
mylist.append(2)
mylist.append(3)
print(mylist[0]) # prints 1 #it goes from 0
print(mylist[1]) # prints 2
print(mylist[2]) # prints 3

for x in mylist:
    print(x)

mylist = [1,2,3]
print(mylist[0])   

#excercise for lists
numbers = []
strings = []
names = ["John", "Eric", "Jessica"]

# write your code here
second_name = None
numbers.append(1)
numbers.append(2)
numbers.append(3)
strings.append("hello")
strings.append("world")
second_name=names[1]

# this code should write out the filled arrays and the second name in the names list (Eric).
print(numbers)
print(strings)
print("The second name on the names list is %s" % second_name)
print(second_name)

#arithmetic operators
number = 1 + 2 * 3 / 4.0
print(number)

remainder = 11 % 3
print(remainder)

squared = 7 ** 2
cubed = 2 ** 3
print(squared)
print(cubed)

helloworld = "hello" + " " + "world"
print(helloworld)

lotsofhellos = "hello" * 10
print(lotsofhellos)

even_numbers = [2,4,6,8]
odd_numbers = [1,3,5,7]
all_numbers = odd_numbers + even_numbers
print(all_numbers)

print([1,2,3] * 3)