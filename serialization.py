import json

with open('data.json') as json_data:
    d = json.loads(json_data.read())
print(d)

json_string = json.dumps([1, 2, 3, "a", "b", "c"])
print(json_string)

import pickle
pickled_string = pickle.dumps([1, 2, 3, "a", "b", "c"])
print(pickle.loads(pickled_string))

json_string = '{"Alfred" : 300, "Jane" : 400 }'
print(json.loads(json_string))
print(type(json.loads(json_string)))
json_string = '[1, 2, 3, "a", "b", "c"]'
print(json.loads(json_string))
print(type(json.loads(json_string)))