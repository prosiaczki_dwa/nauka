# your code goes here
def multiplier_of(number):
    def x(arg):
        return arg
    return x

def f1():
    x=5

def y(arg):
    return arg    
# zle przyklady
# y()
# y('asad', 123)

wynik_1 = y('mis')
print(wynik_1)

nowa_nazwa_dla_jakiejs_funkcji = y
wynik_jakiejs_funkcji = nowa_nazwa_dla_jakiejs_funkcji('wszystko_jedno')
print(wynik_jakiejs_funkcji)
     
f1()
cosik = multiplier_of(5)
#tutaj cosik to wynik funkcji multiplier_of z argumentem 5
#funkcja multiplier_of ma return x w linijce nr 5 
#wiec cosik = x, a x to funkcja ktora zwraca swoj argument tak jak y
#co zrobic zeby cosik mnozyl argument razy 5?
#skoro cosik to x, wiec zeby cosik mnozyl razy 5 to x musi mnozyc razy 5
#a jak napisac funcje ktora mnozy razy 5 to umiemy! patrz funkcja mnozenie!!!
#kolejnym i ostatnim krokiem bedzie zrobienie funkcji ktora nie mnozy zawsze przez 5 tylko zrobienie funkcji ktora pomnozy przez numer
#funkcji zewnetrznej
zmienna = cosik(9)
print(zmienna)
 
 
def mnozenie(arg):
    liczba = arg * 5
    return liczba

wynik = mnozenie(2)
print(wynik)
    
 
 
def zwroc_12(niewazne_jaki_argument_i_tak_go_nie_uzyjemy):
    return 12
 
zmienna_ciezko_sie_nazywa_ale_nie_robi_wiele = zwroc_12
 
nasz_super_tajny_wynik = zmienna_ciezko_sie_nazywa_ale_nie_robi_wiele('Jakis skomplikowany argument')
print("nasz tajny wynik: " + str(nasz_super_tajny_wynik))