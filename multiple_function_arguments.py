def foo(first, second, third, *therest):
    print("First: %s" % first)
    print("Second: %s" % second)
    print("Third: %s" % third)
    print("And all the rest... %s" % list(therest))
    
foo(1,2,3,4,5)

def bar(first, second, third, **options):
    if options.get("action") == "sum":
        print("The sum is: %d" %(first + second + third))

    if options.get("number") == "first":
        return first

result = bar(1, 2, 3, action = "sum", number = "first")
print("Result: %d" %(result))


def funkcja(*aaa, **bbb):
    if(aaa):
        print('printuje z jedna gwiazdka')
        print(aaa)
    if(bbb):
        print('printuje z 2oma gwiazdkami')
        print(bbb)   
    
funkcja()
c='tralala'
funkcja(1,2,c)
funkcja(pierwszy=1,trzeci=3,costam=c)