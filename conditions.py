x = 2
print(x == 2) # prints out True
print(x == 3) # prints out False
print(x < 3) # prints out True

name = "John"
age = 23
if name == "John" and age == 23:
    print("Your name is John, and you are also 23 years old.")

if name == "John" or name == "Rick":
    print("Your name is either John or Rick.")
    
    name = "John"
if name in ["John", "Rick"]:
    print("Your name is either John or Rick.")
    
x = [1,2,3]
y = [1,2,3]
print(x == y) # Prints out True
print(x is y) # Prints out False

lista_a = [1, 2, 3]
lista_c = lista_a
lista_b = [1, 2, 3]

print('test list a b')
print(lista_a == lista_b)
print(lista_a is lista_b)
print('test list a c')
print(lista_a == lista_c)
print(lista_a is lista_c)