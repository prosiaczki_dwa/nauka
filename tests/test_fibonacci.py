import unittest
from excercises.fibonacci import fibonacci
from excercises.fibonacci import fibonacci_2
from excercises.fibonacci import function_passes
from excercises.fibonacci import function_failes

class FibonacciTest(unittest.TestCase):


    def testPass(self):
        function_passes()
    
    def testFail(self):
        self.assertFalse(function_failes())
        
    def test_for_1_fibonacc_should_return_1(self):
        expected = fibonacci(1)
        self.assertEqual(expected, 1)

    def test_for_2_fibonacc_should_return_1(self):
        expected = fibonacci(2)
        self.assertEqual(expected, 1)
        
    def test_for_3_fibonacc_should_return_2(self):
        expected = fibonacci(3)
        self.assertEqual(expected, 2)
        
    def test_for_1_fibonacc2_should_return_1(self):
        expected = fibonacci_2(1)
        self.assertEqual(expected, 1)

    def test_for_2_fibonacc2_should_return_1(self):
        expected = fibonacci_2(2)
        self.assertEqual(expected, 1)
        
    def test_for_3_fibonacc2_should_return_2(self):
        expected = fibonacci_2(3)
        self.assertEqual(expected, 2)