import unittest
from selenium import webdriver

class SeleniumTest(unittest.TestCase):

    def setUp(self):
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        self.browser = webdriver.Chrome(chrome_options=chrome_options)

    def tearDown(self):
        self.browser.close()
        self.browser.quit()
    
    def test_go_to_google_page(self):
        
        self.assertNotEqual(self.browser.title, 'Google')
        self.browser.get("http://www.google.com")
#         self.browser.get("file:///C:/Nauka/webpage/public/index.html")
        self.assertEqual(self.browser.title, 'Google')
        