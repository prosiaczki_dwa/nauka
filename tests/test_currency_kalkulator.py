import unittest
from currency_calculator import CurrencyCalculator

class TestCurrencyCalculator(unittest.TestCase):
    
    def setUp(self):
        self.calculator = CurrencyCalculator()
        
    def testCurrencyCalculatorHasEuroExchangeRate(self):
        assert self.calculator.euro_exchange_rate != None, "calculator euro_exchange_rate is None"
        
    def testCurrencyCalculatorHasPoundExchangeRate(self):
        assert self.calculator.pound_exchange_rate != None, "calculator pound_exchange_rate is None"
        
    def testCurrencyCalculatorSetEuroExchangeRate(self):
        self.calculator.setEuroExchangeRate(4.1)
        assert self.calculator.euro_exchange_rate == 4.1, "calculator euro_exchange_rate did not set correctly"
        
    def testCurrencyCalculatorSetPoundExchangeRate(self):
        self.calculator.setPoundExchangeRate(4.6)
        assert self.calculator.pound_exchange_rate != None, "calculator pound_exchange_rate is None"    
    
    def testCurrencyCalculatorSetPoundsToPLN(self):
        self.calculator.setPoundExchangeRate(4.6)
        assert self.calculator.poundsToPLN(1) == 4.6, "calculator poundsToPLN failed"
        assert self.calculator.poundsToPLN(2) == 9.2, "calculator poundsToPLN failed"
    
if __name__=="__main__":
    unittest.main()