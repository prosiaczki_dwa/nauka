import unittest
import list_comprehensions

class StringTest(unittest.TestCase):


    def test_splitting_sentence(self):
        sentence = "hi Mark"
        result = list_comprehensions.split_string_to_words(sentence)
        
        expected_words = ["hi", "Mark"]
        self.assertEqual(expected_words, result)
        

    def test_count_words_len(self):
        words = "hi Mark"
        expected = [2, 4]
        result = list_comprehensions.count_letters_in_word(words)
        
        self.assertEqual(expected, result)
        
        
#         "the quick brown fox jumps over the lazy dog"
    def test_splitting_sentence_without_the(self):
        sentence = "the quick brown fox jumps over the lazy dog"
        expected = ['quick', 'brown', 'fox', 'jumps', 'over', 'lazy', 'dog']
        
        result = list_comprehensions.split_string_to_words_without_the(sentence) 
        self.assertEqual(expected, result)
        
    def test_count_words_len_without_the(self):
        words = "the quick brown fox jumps over the lazy dog"
        expected = [5, 5, 3, 5, 4, 4, 3]
        result = list_comprehensions.count_letters_in_word_without_the(words)
        self.assertEqual(expected, result)