import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
import os.path

class SeleniumTest(unittest.TestCase):
    
    @classmethod
    def setUpClass(cls):
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        cls.browser = webdriver.Chrome(chrome_options=chrome_options)
        if os.path.isfile("C:/Nauka/webpage/public/index.html"):
            cls.prosiaczki_page_url = "file:///C:/Nauka/webpage/public/index.html"
        else:
            cls.prosiaczki_page_url = "http://prosiaczki_dwa.gitlab.io/webpage/"
        
    @classmethod
    def tearDownClass(cls):
        cls.browser.close()
        cls.browser.quit()
        
    def setUp(self):
        self.browser.get(self.prosiaczki_page_url)

    def test_go_to_main_page(self):
        self.assertEqual(self.browser.title, 'Web page about guinea pigs')
    
    def is_default_value_of_image_naga_file(self):
        image = self.browser.find_element_by_id("myImage")
        self.assertIn("naga.jpg", image.get_attribute('src'))
        
    def test_image_should_change_when_button_is_clicked_on_wszystkie(self):
        button = self.browser.find_element_by_xpath("//button[text()='Wszystkie!!']")
        image = self.browser.find_element_by_id("myImage")
        self.assertNotIn("wszystkie.jpg", image.get_attribute('src'))
        button.click()
        self.assertIn("wszystkie.jpg", image.get_attribute('src'))
        
    def test_image_should_change_when_button_is_clicked_on_rozetke(self):
        button = self.browser.find_element_by_id("rozetka")
        image = self.browser.find_element_by_id("myImage")
        self.assertNotIn("rozetka.jpg", image.get_attribute('src'))
        button.click()
        self.assertIn("rozetka.jpg", image.get_attribute('src'))
        
    def test_image_should_change_when_button_is_clicked_on_gladkowlosa(self):
        button = self.browser.find_element_by_css_selector("#pictures button:nth-child(3)")
        image = self.browser.find_element_by_id("myImage")
        self.assertNotIn("gladkowlosa.jpg", image.get_attribute('src'))
        button.click()
        self.assertIn("gladkowlosa.jpg", image.get_attribute('src'))
        
    def test_image_should_change_when_button_is_clicked_on_karbowana(self):
        button = self.browser.find_element_by_id("karbowana")
        image = self.browser.find_element_by_id("myImage")
        self.assertNotIn("karbowana.jpg", image.get_attribute('src'))
        button.click()
        self.assertIn("karbowana.jpg", image.get_attribute('src')) 
        
    def test_TU_button(self):
        button = self.browser.find_element_by_id("name_printing_button")
        message = self.browser.find_element_by_id("tekst")
        self.assertEqual(message.text, "")
        button.click()
        message2 = self.browser.find_element_by_id("tekst_error")
        self.assertEqual(message2.text, "Prosze podaj jakies imie.")
        
    def test_is_TU_button_printing_proper_name(self):
        button = self.browser.find_element_by_id("name_printing_button")
        name_field = self.browser.find_element_by_xpath("//input[@name='firstname']")
        self.assertEqual(name_field.text, "")
        name_field.send_keys('Kasia')
        button.click()
        field_for_text = self.browser.find_element_by_id("tekst")
        self.assertEqual(field_for_text.text, "Kasia uwielbia prosiaczki!")
        
    def test_if_Nacisnij_mnie_button_is_changing_a_given_message(self):
        button = self.browser.find_element_by_xpath("//button[text()='Nacisnij mnie']")
        text_field = self.browser.find_element_by_id("myHeader")
        self.assertEqual(text_field.text, "Hej!")
        button.click()
        self.assertEqual(text_field.text, "Prosiaczkowego dnia!")
        
    def assert_age_form(self, input_value, expected_message):
        age_field = self.browser.find_element_by_id("age")
        validate_age_text_field = self.browser.find_element_by_id("validate_age_text")
        self.assertEqual(validate_age_text_field.text, "")
        button = self.browser.find_element_by_xpath("//button[text()='Zatwierdz']")
        age_field.send_keys(input_value)
        button.click()
        self.assertEqual(validate_age_text_field.text, expected_message)
        
    def test_age_equal_to_0(self):
        self.assert_age_form(0, "Jest OK! :-D")
        
    def test_age_equal_to_minus_1(self):
        self.assert_age_form(-1, "Wrong input")
        
    def test_age_equal_to_122(self):
        self.assert_age_form(122, "Jest OK! :-D")
        
    def test_age_equal_to_123(self):
        self.assert_age_form(123, "Wrong input")
        
    def test_non_numerical_age(self):
        self.assert_age_form("aa", "Wrong input")
        
    def test_all_radio_buttons_should_be_enabled_by_default(self):
        radio_button_duzym = self.browser.find_element_by_xpath("//input[@value='duzym']")
        radio_button_ogromnym = self.browser.find_element_by_xpath("//input[@value='ogromnym']")
        radio_button_bez_granic = self.browser.find_element_by_xpath("//input[@value='bez granic']")    
        self.assertTrue(radio_button_duzym.is_enabled())
        self.assertTrue(radio_button_ogromnym.is_enabled())
        self.assertTrue(radio_button_bez_granic.is_enabled())
            
    def test_is_radio_button_duzym_selected_by_default(self):
        radio_button_duzym = self.browser.find_element_by_xpath("//input[@value='duzym']")
        radio_button_ogromnym = self.browser.find_element_by_xpath("//input[@value='ogromnym']")
        radio_button_bez_granic = self.browser.find_element_by_xpath("//input[@value='bez granic']")    
        self.assertTrue(radio_button_duzym.is_selected())
        self.assertFalse(radio_button_ogromnym.is_selected())
        self.assertFalse(radio_button_bez_granic.is_selected())
         
    def test_switching_radio_buttons(self):
        radio_button_duzym = self.browser.find_element_by_xpath("//input[@value='duzym']")
        radio_button_ogromnym = self.browser.find_element_by_xpath("//input[@value='ogromnym']")
        radio_button_bez_granic = self.browser.find_element_by_xpath("//input[@value='bez granic']")
        self.assertTrue(radio_button_duzym.is_selected())
        radio_button_ogromnym.click()
        self.assertTrue(radio_button_ogromnym.is_selected())
        radio_button_bez_granic.click()
        self.assertTrue(radio_button_bez_granic.is_selected())
              
    def test_switching_options(self):
        option_przyjazn = self.browser.find_element_by_xpath("//option[@value='przyjazn']")
        option_towarzystwo = self.browser.find_element_by_xpath("//option[@value='towarzystwo']")
        option_pociechy = self.browser.find_element_by_xpath("//option[@value='pociechy']")
        self.assertTrue(option_pociechy.is_selected())
        self.assertFalse(option_przyjazn.is_selected())
        self.assertFalse(option_towarzystwo.is_selected())
        option_przyjazn.click()
        self.assertFalse(option_pociechy.is_selected())
        self.assertTrue(option_przyjazn.is_selected())
        self.assertFalse(option_towarzystwo.is_selected())        
        option_towarzystwo.click()
        self.assertFalse(option_pociechy.is_selected())
        self.assertFalse(option_przyjazn.is_selected())
        self.assertTrue(option_towarzystwo.is_selected()) 
        
    def test_datalist_zalety(self):
        input_field = self.browser.find_element_by_xpath("//input[@name='zaleta']")
        self.assertEqual(input_field.get_attribute("list"), 'zalety')
        datalist = self.browser.find_element_by_id("zalety")
        self.assertEqual(datalist.tag_name, 'datalist')
        datalist_children = datalist.find_elements_by_css_selector("option")
        self.assertEqual(len(datalist_children), 5)
        self.assertEqual(datalist_children[0].get_attribute("value"), "przymilnosc")
        self.assertEqual(datalist_children[1].get_attribute("value"), "absurdalnosc")
        self.assertEqual(datalist_children[2].get_attribute("value"), "apetyt")
        self.assertEqual(datalist_children[3].get_attribute("value"), "uroda")
        self.assertEqual(datalist_children[4].get_attribute("value"), "chec do zabawy")
        
    def test_na_ile_procent_lubisz_prosiaczki(self):
        input_range = self.browser.find_element_by_xpath("//input[@name='a']")
        input_number = self.browser.find_element_by_xpath("//input[@name='b']")
        output_field = self.browser.find_element_by_xpath("//output[@name='x']")
        self.assertEqual(output_field.text,"")
        input_range.send_keys(Keys.RIGHT)
        self.assertEqual(output_field.text,"101")
        input_number.send_keys(Keys.UP)
        self.assertEqual(output_field.text,"102")
        
    def test_button_kliknij_tyle_razy_ile_chcesz_miec_prosiaczkow(self):
        button=self.browser.find_element_by_xpath("//button[text()='Naciskaj!']")
        output_field = self.browser.find_element_by_id("naciskanie")
        self.assertEqual(output_field.text,"0")
        button.click()
        button.click()
        self.assertEqual(output_field.text,"2")
        
    def test_popup_box_with_prosiaczki_rzadza_alert(self):
        button = self.browser.find_element_by_xpath("//button[text()='A teraz nacisnij tutaj!']")
        button.click()
        alert = self.browser.switch_to.alert
        msg=alert.text
        alert.accept()
        self.assertEqual(msg, "Prosiaczki rzadza!")
        
    def form_wpisz_ile_chcialbys_miec_prosiaczkow(self, input_value, output_message):
        input_field = self.browser.find_element_by_id("number_of_piggs")
        text_field = self.browser.find_element_by_id("number_of_piggs_alert")
        button = self.browser.find_element_by_xpath("//button[text()='Pochwal sie']")
        self.assertEqual(text_field.text,"Czy to dobra odpowiedz?")
        input_field.send_keys(input_value)
        button.click()
        self.assertEqual(text_field.text, output_message)
        input_field.clear()
        
    def test_numeber_of_pigs_equal_minus_1(self):
        self.form_wpisz_ile_chcialbys_miec_prosiaczkow(-1, "Zla wartosc!!")
        
    def test_numeber_of_pigs_equal_0(self):
        self.form_wpisz_ile_chcialbys_miec_prosiaczkow(0, "Zla wartosc!!")
        
    def test_numeber_of_pigs_equal_1(self):
        self.form_wpisz_ile_chcialbys_miec_prosiaczkow(1, "Samotna swinka jest smutna...")
    
    def test_numeber_of_pigs_equal_2(self):
        self.form_wpisz_ile_chcialbys_miec_prosiaczkow(2, "Dobrze! Swinki zyja w grupie!")
        
    def test_numeber_of_pigs_equal_3(self):
        self.form_wpisz_ile_chcialbys_miec_prosiaczkow(3, "Dobrze! Swinki zyja w grupie!")
     
    def test_button_change_picture_switching_photo(self):
        picture = self.browser.find_element_by_id("obraz")
        button = self.browser.find_element_by_id("zamiana_zdjecia")
        self.assertIn("piggy_beware.jpg", picture.get_attribute('src'))
        button.click()
        self.assertIn("pros.png", picture.get_attribute('src'))
        
    def test_button_change_picture_switching_photo_by_onclick(self):
        picture = self.browser.find_elements_by_xpath("//img[@src='piggy_beware.jpg']")[1]
        button = self.browser.find_element_by_xpath("//button[contains(@onclick,'foto_change')]")
        self.assertIn("piggy_beware.jpg", picture.get_attribute('src'))
        button.click()
        self.assertIn("pros.png", picture.get_attribute('src'))
        
    def test_changing_text_from_kliknij_mnie_to_dobrego_dnia(self):
        message = self.browser.find_element_by_xpath("//h1[text()='Kliknij mnie!']")
        message.click()
        self.assertEqual(message.text,"Dobrego dnia!")
        
    def test_wpisz_swoje_imie_a_potem_kliknji_gdzies_indziej(self):
        button_as_a_example_to_click_anywhere = self.browser.find_element_by_id("zamiana_zdjecia")
        input_field = self.browser.find_element_by_id("fname")
        input_field.send_keys("marcin")
        self.assertEqual(input_field.get_attribute("value"), "marcin")
        button_as_a_example_to_click_anywhere.click()
        self.assertEqual(input_field.get_attribute("value"), "MARCIN")
        
    def test_najedz_myszka_is_changing_to_czesc_during_mouse_over(self):
        div_input_field = self.browser.find_element_by_xpath("//div[text()='Najedz myszka']")
        hover = ActionChains(self.browser).move_to_element(div_input_field)
        self.assertEqual(div_input_field.text, "Najedz myszka")
        hover.perform()
        self.assertEqual(div_input_field.text, "Misiowo!")
        
    def test_czy_juz_teraz_jestes_milosnikiem_prosiaczkow(self):
        output_field = self.browser.find_element_by_id("test")
        button = self.browser.find_element_by_xpath("//button[text()='Gotowy?']")
        self.assertEqual(output_field.text,"")
        button.click()
        alert = self.browser.switch_to.alert
        msg=alert.text
        alert.accept()
        self.assertEqual(msg, "To jestes milosnikiem prosiaczkow, czy nie?")
        self.assertEqual(output_field.text, "Witaj w klubie!")

    def test_kliknij_tyle_razy_ile_chcesz_miec_prosiczkow(self):
        button = self.browser.find_element_by_xpath("//button[text()='Naciskaj!']")
        output_field = self.browser.find_element_by_id("naciskanie")
        self.assertEqual(output_field.text , "0")
        button.click()
        self.assertEqual(output_field.text , "1")
        button.click()
        self.assertEqual(output_field.text , "2")
        
    def test_field_with_mouse_click_and_release_events(self):
        div_input_field = self.browser.find_element_by_xpath("//div[text()='kliknij']")
        click_and_hold = ActionChains(self.browser).click_and_hold(div_input_field)
        self.assertEqual(div_input_field.text, "kliknij")
        click_and_hold.perform()
        self.assertEqual(div_input_field.text, "Prosiaczkowo!")
        ActionChains(self.browser).release(div_input_field).perform()
        self.assertEqual(div_input_field.text, "Kilknij jeszcze raz!")
        
        
    def test_nacisnij_przycisk_by_zaznaczyc_wszystkie_paragrafy_na_fioletowo(self):
        button = self.browser.find_element_by_xpath("//button[text()='Tutaj']")
        example_paragraph = self.browser.find_element_by_id("number_of_piggs_alert")
        attributeValue = example_paragraph.value_of_css_property("color")
        self.assertEqual(attributeValue, 'rgba(0, 0, 0, 1)')
        button.click()
        attributeValue_after_click = example_paragraph.value_of_css_property("color")
        self.assertEqual(attributeValue_after_click, 'rgba(238, 130, 238, 1)')
        
#     def test_change_me_using_button_above(self):
#         text_field = self.browser.find_element_by_id("ajax_training_div")
#         button = self.browser.find_element_by_xpath("//button[contains(@onclick,'loadDocWithAjax()')]")
#         self.assertEqual(text_field.text, "Change me using button above")
#         button.click()
#         self.assertEqual(text_field.text, "Nigdy sie nie dowiesz!")
#         #czemu nie naciska guzika?
        
        
        
        
    