dict = {"country": ["Brazil", "Russia", "India", "China", "South Africa"],
       "capital": ["Brasilia", "Moscow", "New Dehli", "Beijing", "Pretoria"],
       "area": [8.516, 17.10, 3.286, 9.597, 1.221],
       "population": [200.4, 143.5, 1252, 1357, 52.98] }

import pandas as pd
brics = pd.DataFrame(dict)
print(brics)

# Set the index for brics
brics.index = ["BR", "RU", "IN", "CH", "SA"]

# Print out brics with new index values
print(brics)

# Import the cars.csv data: cars
misie = pd.read_csv('bears.csv', index_col = 0, sep = ";")

# Print out cars
print(misie)
print('!!!!!misie[0:3]')
print(misie[0:3])
print('!!!!!misie.iloc[2]')
print(misie.iloc[2])
print('!!!!misie.loc[[1,2]]')
print(misie.loc[[1,2]])