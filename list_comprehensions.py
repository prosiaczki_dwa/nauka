def split_string_to_words(sentence):
    words = sentence.split()
    return words

def count_letters_in_word(sentence):
    words = sentence.split()
    word_lengths = [len(word) for word in words]
    return word_lengths

def split_string_to_words_without_the(sentence):
    words = sentence.split()
    list_of_words = []
    for word in words:
        if word != "the":
            list_of_words.append(word)
    return list_of_words

def count_letters_in_word_without_the(sentence):
    words = sentence.split()
    list_of_words = []
    for word in words:
        if word != "the":
            list_of_words.append(word)

    word_lengths = [len(word) for word in list_of_words]
    print ("list of lengths")
    return word_lengths


sentence = "the quick brown fox jumps over the lazy dog"
word_lengths = []
words = sentence.split()
for word in words:
    if word != "the":
        word_lengths.append(len(word))
print(words)

sentence = "the quick brown fox jumps over the lazy dog"
words = sentence.split()
word_lengths = [len(word) for word in words if word != "the"]
print(words)
