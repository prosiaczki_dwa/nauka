import json

def add_employee(salaries_json, name, salary):
    #salaries_json jest stringiem
    salaries_dict = json.loads(salaries_json)
    print(type(salaries_dict))
    salaries_dict[name] = salary
    
    salaries_string= json.dumps(salaries_dict)
  
    return salaries_string
# test code
salaries = '{"Alfred" : 300, "Jane" : 400 }'
new_salaries = add_employee(salaries, "Me", 800)
decoded_salaries = json.loads(new_salaries)
print(decoded_salaries["Alfred"])
print(decoded_salaries["Jane"])
print(decoded_salaries["Me"])