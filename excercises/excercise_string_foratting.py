data = ("John", "Doe", 53.44)
format_string = "Hello %s %s. Your current balance is $%s."

print(format_string % data)
print(str(data[2]))

print("%s %f" % (data[2], data[2]))

astring= "Hello world!!"
print("single quotes are ''")
print(len(astring))
print(astring.index("!"))

astring = "Hello world!"
print(astring[3:7])

astring = "Hello world!"
print(astring[::-1])

astring = "Hello world!"
print(astring.upper())
print(astring.lower())

astring = "Hello world!"
print(astring.startswith("Hello"))
print(astring.endswith("asdfasdfasdf"))

astring = "Hello world!"
afewwords = astring.split(" ")
print(afewwords)