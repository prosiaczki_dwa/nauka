# edit the functions prototype and implementation
def foo(a, b, c, *the_rest): #do czego ta gwiazdka?
    return len(the_rest)

def bar(a, b, c, **args): #dwie gwiazdki bo mamy s�owo kluczowe?
    return args["magicnumber"] == 7


# test code
if foo(1,2,3,4) == 1:
    print("Good.")
if foo(1,2,3,4,5) == 2:
    print("Better.")
if bar(1,2,3,magicnumber = 6) == False:
    print("Great.")
if bar(1,2,3,magicnumber = 7) == True:
    print("Awesome!")