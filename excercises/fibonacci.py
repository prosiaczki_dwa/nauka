def function_passes():
    pass

def function_failes():
    return False

def fibonacci(number):
    if number == 1 or number == 2:
        return 1
    return fibonacci(number-1) + fibonacci(number -2)

def fibonacci_2(number):
    if number == 1 or number == 2:
        return 1
    n1 = 1
    n2 = 1
    for i in range(2, number):
        temp = n2
        n2 = n2 + n1
        n1 = temp
    return n2